﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace GuestProfile
{
    // MVVM model гостя
    public class GuestModel: INotifyPropertyChanged
    {
        private DateTime dateValue;
        private bool male = true;
        private bool female;
        private string lastName;
        private string firstName;
        

        public DateTime DateValue
        {
            get { return dateValue; }
            set
            {
                dateValue = value;
                OnPropertyChanged("DateValue");
            }
        }
        public bool Male
        {
            get { return male; }
            set
            {
                male = value;
                OnPropertyChanged("Male");
            }
        }
        public bool Female
        {
            get { return female; }
            set
            {
                female = value;
                OnPropertyChanged("Female");
            }
        }
        public string LastName
        {
            get { return lastName; }
            set
            {
                lastName = value;
                OnPropertyChanged("LastName");
            }
        }
        public string FirstName
        {
            get { return firstName; }
            set
            {
                firstName = value;
                OnPropertyChanged("FirstName");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }


}
