﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Data;
using NUnit.Framework;

namespace GuestProfile
{
    
    [TestFixture, Apartment(ApartmentState.STA)]
    class GuestProfileTest
    {
        // Вдруг пригодится
        //private void SetValue(TextBox textbox, string value)
        //{
        //    TextBoxAutomationPeer peer = new TextBoxAutomationPeer(textbox);
        //    IValueProvider provider = peer.GetPattern(PatternInterface.Value) as IValueProvider;
        //    provider.SetValue(value);
        //}

        GuestProfileWindow view;
        GuestsViewModel viewmodel;

        [TearDown]
        public void TearDown()
        {
            view.Close();
            view.DataContext = null;
            view = null;
            viewmodel = null;
        }

        [Test]
        // Инициализация GuestsViewModel
        public void Init_GuestsViewModel()
        {
            // Arrange
            GuestsViewModel viewModel = new GuestsViewModel();
            // Act

            // Assert
            if (viewModel == null)
                Assert.Fail();
            else
                Assert.Pass();
        }

        [Test]
        // Ввод корректной фамилии GuestsViewModel.EditedLastName
        public void Input1_LastName_GuestsViewModel()
        {
            // Arrange
            GuestsViewModel viewModel = new GuestsViewModel();
            // Act
            string CorrectLastName = "Correct Name";
            viewModel.EditedLastName = CorrectLastName;
            // Assert
            if (viewModel.EditedLastName == CorrectLastName)
                Assert.Pass();
            else
                Assert.Fail();
        }
        [Test]
        // Ввод корректного имени GuestsViewModel.EditedFirstName
        public void Input1_First_GuestsViewModel()
        {
            // Arrange
            GuestsViewModel viewModel = new GuestsViewModel();
            // Act
            string CorrectFirstName = "Correct Name";
            viewModel.EditedFirstName = CorrectFirstName;
            // Assert
            if (viewModel.EditedFirstName == CorrectFirstName)
                Assert.Pass();
            else
                Assert.Fail();
        }
        [Test]
        // Ввод корректных данных в форму WPF
        public void Input_Corret_GuestViewModel()
        {
            // Arrange            
            view = new GuestProfileWindow();
            view.InitializeComponent();
            viewmodel = new GuestsViewModel();
            view.DataContext = viewmodel;
            view.Show();
            // Act

            view.LastNameTextBox.Text = "LastName";
            view.FirstNameTextBox.Text = "FirsName";
            view.DateTextBox.Text = "01.01.1986";
            view.MaleButton.IsChecked = false;
            view.MaleButton.IsChecked = true;

            view.LastNameTextBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            view.FirstNameTextBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            view.DateTextBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            view.Show();
            
            // Assert
            if (viewmodel.CanSave == true)
                Assert.Pass();
            else
                Assert.Fail();
        }
        [Test]
        // Ввод некорректных данных в форму WPF
        public void Input_InCorret_GuestViewModel()
        {
            // Arrange            
            view = new GuestProfileWindow();
            view.InitializeComponent();
            viewmodel = new GuestsViewModel();
            view.DataContext = viewmodel;
            view.Show();
            // Act

            view.LastNameTextBox.Text = "LastName@";
            view.FirstNameTextBox.Text = "FirsName@";
            view.DateTextBox.Text = "01.01.1986@";
            view.MaleButton.IsChecked = false;
            view.MaleButton.IsChecked = true;

            view.LastNameTextBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            view.FirstNameTextBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            view.DateTextBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            view.Show();

            // Assert
            if (viewmodel.CanSave == false)
                Assert.Pass();
            else
                Assert.Fail();
        }

    }
}
