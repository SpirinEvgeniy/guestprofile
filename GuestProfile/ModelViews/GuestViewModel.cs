﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace GuestProfile
{
    // MVVM ViewModel гостя
    class GuestViewModel: INotifyPropertyChanged
    {
        private GuestModel guest;

        public GuestViewModel(GuestModel g)
        {
            guest = g;
        }

        public string FirstName
        {
            get { return guest.FirstName; }
            set
            {
                guest.FirstName = value;
                OnPropertyChanged("FirstName");
            }
        }
        public string LastName
        {
            get { return guest.LastName; }
            set
            {
                guest.LastName = value;
                OnPropertyChanged("LastName");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
