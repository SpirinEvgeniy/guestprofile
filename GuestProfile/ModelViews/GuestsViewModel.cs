﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Windows;
using System.Windows.Input;

namespace GuestProfile
{
    // MVVM ViewModel коллекции гостей
    class GuestsViewModel : INotifyPropertyChanged, IDataErrorInfo
    {
        // Запрещенные к вводу строки
        private List<string> inCoorrectCharSet = new List<string>() { "@", "!" };
        // максимально количество символов в фамилии и имя_отчество
        private const byte maxChar = 100;
        private GuestModel selectedGuest;
        // текущие значения в полях интерфейса           
        private bool male = true;
        private bool female = false;
        private string editedLastName;
        private string editedFirstName;
        private string editedDate;        
        private bool canSave = false; 
        #region поля, связанные со view 

        

        public bool Male
        {
            get { return male; }
            set
            {
                male = value;
                OnPropertyChanged("Male");
            }
        }
        public bool Female
        {
            get { return female; }
            set
            {
                female = value;
                OnPropertyChanged("Female");
            }
        }

        // текущие значения в еэлементах формы
        public string EditedDate
        {
            get { return editedDate; }
            set
            {
                editedDate = value;
                OnPropertyChanged("EditedDate");
            }
        }
        public string EditedLastName
        {
            get { return editedLastName; }
            set
            {
                editedLastName = value;
                OnPropertyChanged("EditedLastName");
            }
        }
        public string EditedFirstName
        {
            get { return editedFirstName; }
            set
            {
                editedFirstName = value;
                OnPropertyChanged("EditedFirstName");
            }
        }
        public bool CanSave
        {
            get { return canSave; }
            set { canSave = value; OnPropertyChanged("CanSave"); }
        } 



        public ObservableCollection<GuestModel> Guests { get; set; }
        public GuestModel SelectedGuest
        {
            get { return selectedGuest; }
            set
            {
                selectedGuest = value;
                OnPropertyChanged("SelectedGuest");
            }
        }
        #endregion

        #region методы
        // команда удаления текущего гостя
        private RelayCommand removeGuest;
        public RelayCommand RemoveGuest
        {
            get
            {
                return removeGuest ??
                    (removeGuest = new RelayCommand(obj =>
                    {
                        GuestModel guest = obj as GuestModel;
                        if (guest != null)
                        {
                            Guests.Remove(guest);
                        }
                    },
                    (obj) => Guests.Count > 0));
            }
        }
        public GuestsViewModel()
        {
            Guests = new ObservableCollection<GuestModel>
            {
                new GuestModel {LastName="Иванов", FirstName="Иван"},
                new GuestModel {LastName="Петров", FirstName="Петр"},
                new GuestModel {LastName="Сидоров", FirstName="Сидор"}
            };
        }
        // команда добавления нового гостя
        private RelayCommand addGuest;
        public RelayCommand AddGuest
        {
            get
            {
                return addGuest ??
                  (addGuest = new RelayCommand(obj =>
                  {
                      GuestModel guest = new GuestModel();
                      guest.DateValue = DateTime.ParseExact(editedDate, "dd.MM.yyyy", CultureInfo.InvariantCulture);
                      guest.Male = male;
                      guest.Female = female;
                      guest.LastName = editedLastName;
                      guest.FirstName = editedFirstName;
                      Guests.Insert(0, guest);
                      SelectedGuest = guest;
                  }));
            }
        }
        // команда закрытия окна
        private RelayCommand closeApp;
        public RelayCommand CloseApp
        {
            get
            {
                return closeApp ??
                  (closeApp = new RelayCommand(obj =>
                  {
                      Window window = obj as Window;
                      window.Close();
                  }));
            }
        }
        #endregion


        #region реализация интерфейсов
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }       

        // валидация данных
        public string this[string columnName]
        {
            get
            {
                CanSave = false;
                
                string error = String.Empty;
                switch (columnName)
                {
                    case "EditedLastName":
                        {
                            
                            if (EditedLastName != null && (ContainsAny(EditedLastName, inCoorrectCharSet) || EditedLastName.Length > maxChar || EditedLastName.Length == 0))
                                error = "Некорректный ввод";
                            
                        }
                        break;
                    case "EditedFirstName":
                        {
                           
                            if (EditedFirstName != null && (ContainsAny(EditedFirstName, inCoorrectCharSet) || EditedFirstName.Length > maxChar || EditedFirstName.Length == 0))
                                error = "Некорректный ввод";
                            
                        }
                        break;
                    case "EditedDate":
                        {
                            
                            if (!ValidateDate(editedDate))
                                error = "Некорректный ввод";
                        }
                        break;
                }
                CanSave = error == String.Empty;
                return error;
            }
        }
        public string Error
        {
            get { throw new NotImplementedException(); }
        }
        // проверка даты
        public bool ValidateDate(string value)
        {
            bool result = false;
            try
            {
                Regex regex = new Regex(@"^([0]?[0-9]|[12][0-9]|[3][01])[./-]([0]?[1-9]|[1][0-2])[./-]([0-9]{4})$");

                //DateTime date;

                bool isValid = regex.IsMatch(value);

                //isValid = isValid && DateTime.TryParseExact(value, "dd/MM/yyyy", new CultureInfo("en-GB"), DateTimeStyles.None, out date);

                result = isValid;
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }
        // проверка на запрещенные символы
        public static bool ContainsAny(string stringToTest, List<string> substrings)
        {
            if (string.IsNullOrEmpty(stringToTest) || substrings == null)
                return false;

            foreach (var substring in substrings)
            {
                if (stringToTest.Contains(substring))
                    return true;
            }
            return false;
        }
        #endregion
    }

}
